package org.tuxjitl;

import org.tuxjitl.keyboardutility.keyboardinput.KeyboardUtility;
import org.tuxjitl.keyboardutility.menudecoration.MenuUtility;
import org.tuxjitl.model.MenuChoices;
import org.tuxjitl.model.Property;
import org.tuxjitl.repository.IProperty;
import org.tuxjitl.repository.PropertyImpl;

import java.util.List;

public class ImmoApp {

    private static IProperty propRepository = new PropertyImpl();

    public static void main(String[] args) {

        menuHeader();

        mainMenu();

        PropertyImpl.emf.close();

    }

    private static void mainMenu() {

        int choice = -1;
        String[] choices;
        do {
            choices = MenuChoices.mainMenuChoices();
            choice = KeyboardUtility.askForChoice("Make a choice", choices);
            mainMenuDisplay(choice);
            System.out.println(MenuUtility.thickLine());
        } while (choice != choices.length - 1);
    }

    private static void menuHeader() {

        System.out.println(MenuUtility.doubleThinLine());
        System.out.println(MenuUtility.center("IMMO APP"));
        System.out.println(MenuUtility.doubleThinLine());
    }

    private static void mainMenuDisplay(int c) {

        switch (c) {
            case 0:
                propRepository.viewList().stream().forEach(System.out::println);
                break;
            case 1:
                propRepository.searchByAddress(KeyboardUtility.ask("Enter address"))
                        .stream().forEach(System.out::println);
                break;
            case 2:
                propRepository.createNewProperty();
                break;
            case 3:

                List<Property> toUpdateProperty = propRepository.searchByAddress(KeyboardUtility.ask("Enter address to update"));
                if (toUpdateProperty.size() == 0) {
                    System.out.println("No record found to update");
                    break;
                }
                else if (toUpdateProperty.size() == 1) {
                    Property property = toUpdateProperty.get(0);
                    propRepository.updateProperty(property);
                    break;
                }
                else {
                    operationOnMultipleRecs(toUpdateProperty, c);
                    break;
                }
            case 4:
                List<Property> toDeleteProperty = propRepository.searchByAddress(KeyboardUtility.ask("Enter address to delete"));
                if (toDeleteProperty.size() == 0) {
                    System.out.println("No record found to delete");
                    break;
                }
                else if (toDeleteProperty.size() == 1) {
                    Property property = toDeleteProperty.get(0);
                    propRepository.deleteProperty(property);
                    break;
                }
                else {

                    operationOnMultipleRecs(toDeleteProperty,c);
                    break;
                }

            case 5:
                System.out.println("Closing Immo App. See you next time.");
                System.exit(0);
        }
    }

    private static void operationOnMultipleRecs(List<Property> properties, int aOperation) {

        String[] multipleRecs = new String[properties.size()];

        for (int i = 0; i < properties.size(); i++) {
            multipleRecs[i] = properties.get(i).toString();
        }

        if(aOperation == 3){
            Property property = properties.get(KeyboardUtility.askForChoice("Which property do you want to update?", multipleRecs));
            propRepository.updateProperty(property);
        }else{
            Property property = properties.get(KeyboardUtility.askForChoice("Which property do you want to delete?", multipleRecs));
            propRepository.deleteProperty(property);

        }

    }
}
