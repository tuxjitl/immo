package org.tuxjitl.repository;


import org.tuxjitl.keyboardutility.keyboardinput.KeyboardUtility;
import org.tuxjitl.keyboardutility.menudecoration.MenuUtility;
import org.tuxjitl.model.EPC;
import org.tuxjitl.model.MenuChoices;
import org.tuxjitl.model.Property;
import org.tuxjitl.model.PropertyType;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.util.List;

public class PropertyImpl implements IProperty {

    public static EntityManagerFactory emf = Persistence.createEntityManagerFactory("ImmoApp");
    static EntityManager em = emf.createEntityManager();

    String[] types = MenuChoices.getEnumNames(PropertyType.class);
    String[] columnNames = getColumnNamesOfTable();
    String[] epcs = MenuChoices.getEnumNames(EPC.class);
    int chosenType = -1;
    int chosenEPC = -1;

    @Override
    public void updateProperty(Property property) {

        int choice = KeyboardUtility.askForChoice("What do you want to update?",columnNames);

        switch (choice){
            case 0:
                em.getTransaction().begin();
                property.setAddress(KeyboardUtility.ask("Enter new address"));
                em.getTransaction().commit();
                break;
            case 1:
                em.getTransaction().begin();
                property.setPrice(KeyboardUtility.askForDouble("Enter new price"));
                em.getTransaction().commit();
                break;
            case 2:
                em.getTransaction().begin();
                property.setType(PropertyType.values()[KeyboardUtility.askForChoice("Choose Property type",types)]);
                em.getTransaction().commit();
                break;
            case 3:
                em.getTransaction().begin();
                property.setEpc(EPC.values()[KeyboardUtility.askForChoice("Choose EPC",epcs)]);
                em.getTransaction().commit();
                break;
            case 4:
                em.getTransaction().begin();
                property.setInteriorArea(KeyboardUtility.askForInt("Enter new interior area"));
                em.getTransaction().commit();
                break;
            case 5:
                em.getTransaction().begin();
                property.setPlotArea(KeyboardUtility.askForInt("Enter new plot area"));
                em.getTransaction().commit();
                break;
            case 6:
                em.getTransaction().begin();
                property.setNrBedrooms(KeyboardUtility.askForInt("Enter new nr of bedrooms"));
                em.getTransaction().commit();
                break;
            case 7:
                em.getTransaction().begin();
                property.setNrBathrooms(KeyboardUtility.askForInt("Enter new nr of bathrooms"));
                em.getTransaction().commit();
                break;
            case 8:
                em.getTransaction().begin();

                property.setAddress(KeyboardUtility.ask("Enter new address"));
                property.setPrice(KeyboardUtility.askForDouble("Enter new price"));

                chosenType = (KeyboardUtility.askForChoice("Choose Property type",types));
                property.setType(PropertyType.values()[chosenType]);

                chosenEPC = (KeyboardUtility.askForChoice("Choose EPC",epcs));
                property.setEpc(EPC.values()[chosenEPC]);

                property.setInteriorArea(KeyboardUtility.askForInt("Enter new interior area"));
                property.setPlotArea(KeyboardUtility.askForInt("Enter new plot area"));
                property.setNrBedrooms(KeyboardUtility.askForInt("Enter new nr of bedrooms"));
                property.setNrBathrooms(KeyboardUtility.askForInt("Enter new nr of bathrooms"));

                em.getTransaction().commit();

            default:
                 break;
        }

    }

    @Override
    public void deleteProperty(Property property) {

        em.getTransaction().begin();
        em.remove(property);
        em.getTransaction().commit();
    }

    @Override
    public List<Property> viewList() {

        seperationVisual();

        String strQuery = "SELECT p FROM Property p";
        TypedQuery<Property> query = em.createQuery(strQuery, Property.class);
        return query.getResultList();

    }

    @Override
    public void createNewProperty() {

        seperationVisual();

        String address = KeyboardUtility.ask("Enter address");
        double price = KeyboardUtility.askForDouble("Enter price");

        PropertyType type = PropertyType.values()[KeyboardUtility.askForChoice("Choose Property type", types)];

        EPC epc = EPC.values()[KeyboardUtility.askForChoice("Choose EPC category", epcs)];

        int interiorArea = KeyboardUtility.askForInt("Enter interior area");
        int plotArea = KeyboardUtility.askForInt("Enter plot area");
        int nrBedRooms = KeyboardUtility.askForInt("Enter nr of bed rooms");
        int nrBathRooms = KeyboardUtility.askForInt("Enter nr of bath rooms");

        Property property = new Property(address, price, type, epc, interiorArea, plotArea, nrBedRooms, nrBathRooms);
        save(property);

    }

    private void seperationVisual() {

        System.out.println(MenuUtility.thinLine());
        System.out.println();
    }

    @Override
    public void save(Property property) {

        em.getTransaction().begin();
        em.persist(property);
        em.getTransaction().commit();
    }

    @Override
    public List<Property> searchByAddress(String searchTerm) {

        String strQuery = "SELECT p FROM Property p WHERE p.address LIKE :searchTerm";
        TypedQuery<Property> query = em.createQuery(strQuery, Property.class);
        query.setParameter("searchTerm", "%" + searchTerm + "%");

        return query.getResultList();
    }


    @Override
    public String[] getColumnNamesOfTable() {

        Field[] fields = Property.class.getDeclaredFields();
        String[] columnNames = new String[fields.length];

        for (int i = 1; i < fields.length;i++){
            columnNames[i-1] = fields[i].getName();
        }

        columnNames[columnNames.length-1]="Update All";
        return columnNames;
    }
}
