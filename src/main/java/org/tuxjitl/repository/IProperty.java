package org.tuxjitl.repository;

import org.tuxjitl.model.Property;

import java.util.List;

public interface IProperty {
    void createNewProperty();
    void save(Property property);
    List<Property> searchByAddress(String searchTerm);
    void updateProperty(Property property);
    void deleteProperty(Property property);
    List<Property> viewList();
    String[] getColumnNamesOfTable();

}
