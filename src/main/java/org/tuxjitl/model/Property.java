package org.tuxjitl.model;

import javax.persistence.*;

@Entity
public class Property {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String address;
    private double price;
    @Enumerated(EnumType.STRING)
    private PropertyType type;
    @Enumerated(EnumType.STRING)
    private EPC epc;
    @Column(name = "interior_area")
    private int interiorArea;
    @Column(name = "plot_area")
    private int plotArea;
    @Column(name = "nr_bedrooms")
    private int nrBedrooms;
    @Column(name = "nr_bathrooms")
    private int nrBathrooms;

    public Property() {

    }

    public Property(String address, double price, PropertyType type, EPC epc, int interiorArea, int plotArea,
                    int nrBedrooms, int nrBathrooms) {

        this.address = address;
        this.price = price;
        this.type = type;
        this.epc = epc;
        this.interiorArea = interiorArea;
        this.plotArea = plotArea;
        this.nrBedrooms = nrBedrooms;
        this.nrBathrooms = nrBathrooms;
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {

        this.address = address;
    }

    public double getPrice() {

        return price;
    }

    public void setPrice(double price) {

        this.price = price;
    }

    public PropertyType getType() {

        return type;
    }

    public void setType(PropertyType type) {

        this.type = type;
    }

    public EPC getEpc() {

        return epc;
    }

    public void setEpc(EPC epc) {

        this.epc = epc;
    }

    public int getInteriorArea() {

        return interiorArea;
    }

    public void setInteriorArea(int interiorArea) {

        this.interiorArea = interiorArea;
    }

    public int getPlotArea() {

        return plotArea;
    }

    public void setPlotArea(int plotArea) {

        this.plotArea = plotArea;
    }

    public int getNrBedrooms() {

        return nrBedrooms;
    }

    public void setNrBedrooms(int nrBedrooms) {

        this.nrBedrooms = nrBedrooms;
    }

    public int getNrBathrooms() {

        return nrBathrooms;
    }

    public void setNrBathrooms(int nrBathrooms) {

        this.nrBathrooms = nrBathrooms;
    }

    @Override
    public String toString() {

        return "Property{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", price=" + price +
                ", type=" + type +
                ", epc=" + epc +
                ", interiorArea=" + interiorArea +
                ", plotArea=" + plotArea +
                ", nrBedrooms=" + nrBedrooms +
                ", nrBathrooms=" + nrBathrooms +
                '}';
    }
}
