package org.tuxjitl.model;

public enum PropertyType {
    HUIS("HUIS"),
    APPARTEMENT("APPARTEMENT"),
    VAKANTIEHUIS("VAKANTIEHUIS"),
    CHALET("CHALET");

    private final String strType;

    private PropertyType(String s){
        strType = s;
    }

    public boolean equalsName(String otherType) {
        // (otherType == null) check is not needed because strType.equals(null) returns false
        return strType.equals(otherType);
    }

    public String toString() {
        return this.strType;
    }

}
