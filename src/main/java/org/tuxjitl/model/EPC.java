package org.tuxjitl.model;

public enum EPC {

    A("A"),
    B("B"),
    C("C"),
    D("D"),
    E("E"),
    F("F");


    private final String strECP;

    private EPC(String s){
        strECP = s;
    }

    public boolean equalsName(String otherECP) {
        // (otherECP == null) check is not needed because strECP.equals(null) returns false
        return strECP.equals(otherECP);
    }

    public String toString() {
        return this.strECP;
    }
}
