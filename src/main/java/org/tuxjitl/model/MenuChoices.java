package org.tuxjitl.model;

import java.util.ArrayList;
import java.util.Arrays;

public class MenuChoices {

    private static ArrayList<String> menu = new ArrayList<>();


    public static String[] mainMenuChoices(){
        menu.clear();
        menu.add("View List");
        menu.add("Search by Address");
        menu.add("Add new property");
        menu.add("Update property");
        menu.add("Delete property");
        menu.add("Exit");

        String[] choices = new String[menu.size()];
        for(int i = 0; i< menu.size();i++){
            choices[i] = menu.get(i);
        }

        return choices;
    }

    public static String[] getEnumNames(Class<? extends Enum<?>> e) {
        return Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
    }



}
